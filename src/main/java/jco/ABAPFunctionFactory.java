package jco;

import com.sap.conn.jco.JCoDestination;
import com.sap.conn.jco.JCoDestinationManager;
import com.sap.conn.jco.JCoException;

public class ABAPFunctionFactory {

	/**
	 * @param args
	 * @throws JCoException 
	 */
	
	public ABAPFunction getAbapFunction() throws JCoException{
		
		JCoDestination destination = JCoDestinationManager.getDestination("JCO");
		ABAPFunction abapFunction = new ABAPFunction( destination );
		
		return abapFunction;
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
