package jco;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Properties;

public class JCoDataFileGenerator {

	/**
	 * @param args
	 */
	JCoPropertyMaker jcoPropertyMaker;
	
	JCoDataFileGenerator(JCoPropertyMaker p_jcoPropertyMaker){
		jcoPropertyMaker = p_jcoPropertyMaker;
	}
	
	public void genDataFile( ){
		Properties connectProperties = jcoPropertyMaker.getConnectionProperties(); 
		createDataFile("JCO", "JCoDestination", connectProperties);
	}
	
	static void createDataFile(String name, String suffix, Properties properties) {
		File cfg = new File(name + "." + suffix);

		if (!cfg.exists()) {
			try {
				FileOutputStream fos = new FileOutputStream(cfg, false);
				properties.store(fos, "for tests only !");
				fos.close();
			} catch (Exception e) {
				throw new RuntimeException(
						"Unable to create the destination file "
								+ cfg.getName(), e);
			}
		}
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
