package jco;

import com.sap.conn.jco.AbapException;
import com.sap.conn.jco.JCoDestination;
import com.sap.conn.jco.JCoException;
import com.sap.conn.jco.JCoFunction;

public class ABAPFunction {

	/**
	 * @param args
	 */

	private JCoDestination destination;

	public ABAPFunction(JCoDestination p_destination) {
		destination = p_destination;
	}

	
	public JCoDestination getDestination() {
		return destination;
	}


	public JCoFunction getJCoFunction(String p_functionName)
			throws JCoException {
		JCoFunction function = destination.getRepository().getFunction(
				p_functionName);
		if (function == null)
			throw new RuntimeException(p_functionName + " not found in SAP.");
		
		try {
			function.execute(destination);
		} catch (AbapException e) {
			System.out.println(e.toString());
			// return;
		}

		return function;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
