package jco;


import java.sql.SQLException;
import java.util.Properties;

public interface JCoPropertyMaker {
	public Properties getConnectionProperties();
}
