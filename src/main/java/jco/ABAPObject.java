package jco;

import com.sap.conn.jco.JCoFunction;
import com.sap.conn.jco.JCoStructure;
import com.sap.conn.jco.JCoTable;

public class ABAPObject {

	/**
	 * @param args
	 */
	
	JCoFunction function;
	
	ABAPObject( JCoFunction p_function ){
		function = p_function;
	}
	
	public JCoStructure getJCoStructure( String p_structureName ){
		JCoStructure returnStructure = function.getExportParameterList().getStructure(p_structureName);
		
		return returnStructure;
	}
	
	public JCoTable getJCoTable( String p_tableName ){
		JCoTable codes = function.getTableParameterList().getTable( p_tableName );
		
		return codes;
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}
	
	

}
