package jco;

import com.sap.conn.jco.JCoDestination;
import com.sap.conn.jco.JCoDestinationManager;
import com.sap.conn.jco.JCoException;
import com.sap.conn.jco.JCoFunction;

public class ABAPObjectFactory {

	/**
	 * @param args
	 * @throws JCoException 
	 */
	
	public ABAPObject getABAPObject( String p_functionName ) throws JCoException{
		ABAPFunctionFactory abapFunctionFactory = new ABAPFunctionFactory();
		JCoFunction function = new ABAPFunctionFactory().getAbapFunction().getJCoFunction( p_functionName );
		
		return new ABAPObject( function);
	}
}
