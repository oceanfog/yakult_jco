package jco;

import java.util.ArrayList;

import com.sap.conn.jco.AbapException;
import com.sap.conn.jco.JCoDestination;
import com.sap.conn.jco.JCoDestinationManager;
import com.sap.conn.jco.JCoException;
import com.sap.conn.jco.JCoFunction;
import com.sap.conn.jco.JCoStructure;
import com.sap.conn.jco.JCoTable;

public class JCoDaoTester {

	/**
	 * @param args
	 * @throws JCoException 
	 */
	public static void main(String[] args) throws JCoException {
		// TODO Auto-generated method stub
		
		
		//Destination File 생성
		JCoDataFileGeneratorFactory jcdFileGenFactory = new JCoDataFileGeneratorFactory();
		jcdFileGenFactory.jcoDataFileGenerator().genDataFile();
		
		JCoDao jcd = new JCoDao();
//		JCoStructure jcoStructure = jcd.getJCoStructure("RFC_SYSTEM_INFO", "RFCSI_EXPORT" );
//		
//		jcd.printStructureFields(jcoStructure);
		
		JCoTable jcoTable = jcd.getJCoTable("BAPI_COMPANYCODE_GETLIST", "COMPANYCODE_LIST" );
		
//		ArrayList<String> arl = jcd.step4WorkWithTable();
//
//		for (int i = 0; i < arl.size(); i++) {
//			System.out.println(arl.get(i));
//		}

//		jcd.runJcoFunctionTester();
		
		
		JCoDestination destination = JCoDestinationManager.getDestination("JCO");
		
		ABAPFunction abapFunction = new ABAPFunctionFactory().getAbapFunction(); 
		JCoFunction function = abapFunction.getJCoFunction("BAPI_COMPANYCODE_GETDETAIL");
		
		function.getImportParameterList().setValue("COMPANYCODEID", "KCC1");
		
		function.getExportParameterList().setActive("COMPANYCODE_ADDRESS",false);
		
		function.execute(abapFunction.getDestination());
		
		ABAPObject abapObject = new ABAPObject(function);
		
		JCoStructure returnStructure =  abapObject.getJCoStructure("RETURN");
		
		JCoStructure detail = abapObject.getJCoStructure("COMPANYCODE_DETAIL");
		
		System.out.println(detail.getString("COMP_CODE"));
		
		
		
		
	}

}

