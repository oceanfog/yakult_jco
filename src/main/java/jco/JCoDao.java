package jco;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Properties;

import com.sap.conn.jco.AbapException;
import com.sap.conn.jco.JCoDestination;
import com.sap.conn.jco.JCoDestinationManager;
import com.sap.conn.jco.JCoException;
import com.sap.conn.jco.JCoField;
import com.sap.conn.jco.JCoFunction;
import com.sap.conn.jco.JCoStructure;
import com.sap.conn.jco.JCoTable;
import com.sap.conn.jco.ext.DestinationDataProvider;

public class JCoDao {
	static String ABAP_AS = "JCO";
	static String ABAP_MS = "JCO";
	
	public JCoDao() {

	}

	public JCoStructure getJCoStructure(String p_functionName, String p_structureName) throws JCoException {
		
		ABAPFunction abapFunction = new ABAPFunctionFactory().getAbapFunction();
		JCoFunction function = abapFunction.getJCoFunction(p_functionName);
		JCoStructure exportStructure = new ABAPObject(function).getJCoStructure( p_structureName );
		
		return exportStructure;
	}
	
	public void printStructureFields( JCoStructure exportStructure ){
		for (JCoField field : exportStructure) {
			System.out.println(field.getName() + ":\t" + field.getString());
		}

	}

	
	public void runJcoFunctionTester() throws JCoException {
		ArrayList r_arl = new ArrayList();
		JCoFunction function = new ABAPFunctionFactory().getAbapFunction().getJCoFunction("BAPI_COMPANYCODE_GETLIST");
		
		JCoDestination destination = ((ABAPFunction) function).getDestination();
		
		try {
			function.execute(destination);
		} catch (AbapException e) {
			System.out.println(e.toString());
			// return;
		}

		JCoStructure returnStructure = new ABAPObject( function ).getJCoStructure( "RETURN" );
		JCoTable codes = function.getTableParameterList().getTable("COMPANYCODE_LIST");
		
		for (int i = 0; i < codes.getNumRows(); i++) {
			codes.setRow(i);
			// System.out.println(codes.getString("COMP_CODE") + '\t'
			// + codes.getString("COMP_NAME"));

			r_arl.add(codes.getString("COMP_CODE"));
		}

		codes.firstRow();
		for (int i = 0; i < codes.getNumRows(); i++, codes.nextRow()) {
			function = destination.getRepository().getFunction(
					"BAPI_COMPANYCODE_GETDETAIL");

			if (function == null) {
				throw new RuntimeException(
						"BAPI_COMPANYCODE_GETDETAIL not found in SAP.");
			}
		}

		function.getImportParameterList().setValue("COMPANYCODEID",
				codes.getString("COMP_CODE"));
		function.getExportParameterList().setActive("COMPANYCODE_ADDRESS",
				false);
		try {
			function.execute(destination);
		} catch (AbapException e) {
			System.out.println(e.toString());
			return;
		}
		returnStructure = new ABAPObject( function ).getJCoStructure( "RETURN" );
		
		JCoStructure detail = function.getExportParameterList().getStructure(
				"COMPANYCODE_DETAIL");
		System.out.println(detail.getString("COMP_CODE") + '\t'
						+ detail.getString("COUNTRY") + '\t'
						+ detail.getString("CITY"));

	}

		
	public ArrayList step4WorkWithTable() throws JCoException {
		ArrayList r_arl = new ArrayList();
		
		
		JCoTable codes = getJCoTable("BAPI_COMPANYCODE_GETLIST", "COMPANYCODE_LIST");
		
		for (int i = 0; i < codes.getNumRows(); i++) {
			codes.setRow(i);
			System.out.println(codes.getString("COMP_CODE") + '\t'	+ codes.getString("COMP_NAME"));

			r_arl.add(codes.getString("COMP_CODE"));
		}

		return r_arl;
	}
	
	
	
	public JCoTable getJCoTable(String p_functionName, String p_tableName) throws JCoException{
		ABAPFunction abapFunction = new ABAPFunctionFactory().getAbapFunction();
		JCoFunction function = abapFunction.getJCoFunction(p_functionName );
		JCoDestination destination = abapFunction.getDestination();
		
		
		return new ABAPObject( function ).getJCoTable(p_tableName);
	}
	

	public static void main(String[] args) throws JCoException {
		
	}
}
